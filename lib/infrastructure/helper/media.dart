import 'dart:io';

import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

class MediaProcess {
  static Future<File?> getImageFromGallery({
    bool crop = false,
    CropAspectRatio aspectRatio = const CropAspectRatio(
      ratioX: 1,
      ratioY: 1,
    ),
  }) async {
    final picker = ImagePicker();
    final pickImage = await picker.pickImage(source: ImageSource.gallery);
    if (crop && pickImage != null) {
      final cropper = await ImageCropper.cropImage(
        sourcePath: pickImage.path,
        aspectRatio: aspectRatio,
      );
      return cropper;
    }

    if (pickImage == null) return null;
    final file = File(pickImage.path);
    return file;
  }

  static Future<File?> getImageFromCamera({
    bool crop = false,
    CropAspectRatio aspectRatio = const CropAspectRatio(
      ratioX: 1,
      ratioY: 1,
    ),
  }) async {
    final picker = ImagePicker();
    final pickImage = await picker.pickImage(source: ImageSource.camera);

    if (pickImage == null) {
      await picker.retrieveLostData();
    }

    if (crop && pickImage != null) {
      final cropper = await ImageCropper.cropImage(
        sourcePath: pickImage.path,
        aspectRatio: aspectRatio,
      );
      return cropper;
    }

    if (pickImage == null) return null;
    return File(pickImage.path);
  }
}
