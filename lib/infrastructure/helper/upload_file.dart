import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart' as fs;
import 'package:intl/intl.dart';

Future<String> upload(File file) async {
  const destRef = 'uploads';
  try {
    final storage = fs.FirebaseStorage.instance;
    final ext = file.path.split('.').last;
    final unique = DateFormat('hhmmss').format(DateTime.now());
    final filename = '$unique.$ext';
    final fileRef = '$destRef/$filename';
    final upload = await storage.ref(fileRef).putFile(file);
    return upload.ref.getDownloadURL();
  } catch (e) {
    return '';
  }
}
