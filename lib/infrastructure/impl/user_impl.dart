import 'dart:convert';

import 'package:alesha/domain/user/exception/user_exception.dart';
import 'package:alesha/domain/user/user.dart';
import 'package:alesha/injection.dart';
import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart' as fa;
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'package:injectable/injectable.dart';

@Injectable(as: UserRepo, env: Env.all)
class UserImpl implements UserRepo {
  @override
  Future<Either<UserException, List<Doctor>>> doctors({
    String name = '',
  }) async {
    try {
      final uri = Uri.parse('https://jsonplaceholder.typicode.com/users');
      final resp = await http.get(
        uri,
        headers: {
          'Content-type': 'application/json',
        },
      );

      if (resp.statusCode != 200) {
        throw UserException('Failed to fetch doctor');
      }
      final bodyString = resp.body;
      final body = jsonDecode(bodyString) as List;
      final parsed = body.map<Doctor>((dynamic e) {
        final singleRawData = e as Map<String, dynamic>;
        return Doctor(
          id: singleRawData['id'].toString(),
          location: singleRawData['address']['city'] as String,
          name: singleRawData['name'] as String,
          email: singleRawData['email'] as String,
        );
      }).toList();
      return right(parsed);
    } catch (e) {
      return left(UserException(e.toString()));
    }
  }

  @override
  Future<Either<UserException, void>> googleLogin() async {
    try {
      // Trigger the authentication flow
      final googleUser = await GoogleSignIn().signIn();

      // Obtain the auth details from the request
      final googleAuth = await googleUser?.authentication;

      // Create a new credential
      final credential = fa.GoogleAuthProvider.credential(
        accessToken: googleAuth?.accessToken,
        idToken: googleAuth?.idToken,
      );

      // Once signed in, return the UserCredential
      await fa.FirebaseAuth.instance.signInWithCredential(credential);
      return right(null);
    } catch (e) {
      return left(UserException(e.toString()));
    }
  }

  @override
  Future<Either<UserException, void>> logout() async {
    try {
      await fa.FirebaseAuth.instance.signOut();
      return right(null);
    } catch (e) {
      return left(UserException(e.toString()));
    }
  }

  @override
  Future<Either<UserException, List<Appointment>>> myAppointment() {
    // TODO: implement myAppointment
    throw UnimplementedError();
  }

  @override
  Future<Either<UserException, User>> profile() async {
    try {
      final user = fa.FirebaseAuth.instance.currentUser;
      if (user == null) {
        throw UserException('No user');
      }
      await user.reload();
      return right(User.fromFirebaseUser(user));
    } catch (e) {
      return left(UserException(e.toString()));
    }
  }

  @override
  Future<Either<UserException, void>> traditionalLogin(
    String email,
    String password,
  ) async {
    try {
      await fa.FirebaseAuth.instance.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      return right(null);
    } on fa.FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        return left(UserException('No user found for that email.'));
      } else if (e.code == 'wrong-password') {
        return left(UserException('Wrong password provided for that user.'));
      }
      return left(UserException(e.message!));
    } catch (e) {
      return left(UserException(e.toString()));
    }
  }

  @override
  Future<Either<UserException, User>> updateProfile({
    String? name,
    String? email,
    String? photo,
  }) async {
    try {
      final currentUser = fa.FirebaseAuth.instance.currentUser;

      if (name != null) {
        await currentUser!.updateDisplayName(name);
      }

      if (email != null) {
        await currentUser!.updateEmail(email);
      }

      if (photo != null) {
        await currentUser!.updatePhotoURL(photo);
      }

      await currentUser!.reload();

      return right(User.fromFirebaseUser(currentUser));
    } catch (e) {
      return left(UserException(e.toString()));
    }
  }

  @override
  Future<Either<UserException, void>> register({
    required String name,
    required String email,
    required String password,
  }) async {
    try {
      final c = await fa.FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );

      await c.user!.updateDisplayName(name);
      await c.user!.reload();
      return right(null);
    } on fa.FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        return left(
          UserException('The password provided is too weak.'),
        );
      } else if (e.code == 'email-already-in-use') {
        return left(
          UserException('The account already exists for that email.'),
        );
      }
      return left(UserException(e.message!));
    } catch (e) {
      return left(UserException(e.toString()));
    }
  }
}
