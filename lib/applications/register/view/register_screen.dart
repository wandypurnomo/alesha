import 'package:alesha/applications/app/app.dart';
import 'package:alesha/applications/dashboard/dashboard.dart';
import 'package:alesha/applications/register/register.dart';
import 'package:alesha/injection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:velocity_x/velocity_x.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<RegisterBloc>(),
      child: const _RegisterScreen(),
    );
  }
}

class _RegisterScreen extends StatelessWidget {
  const _RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final c = context.read<RegisterBloc>();
    return Scaffold(
      body: BlocListener<AuthBloc, AuthState>(
        listener: (context, state) {
          if (state is AuthSuccess) {
            context.nextAndRemoveUntilPage(const DashboardScreen());
          }
        },
        child: Form(
          key: c.formKey,
          child: VStack(
            [
              const SizedBox(height: 40),
              HStack(
                [
                  IconButton(
                    onPressed: () => context.pop<dynamic>(),
                    icon: const Icon(Icons.arrow_back_ios),
                  ),
                  VStack(
                    [
                      'Sign Up'.text.size(22).bold.end.make(),
                      'create new account'
                          .text
                          .size(16)
                          .color(const Color(0xFF8A8D9F))
                          .end
                          .make(),
                    ],
                    crossAlignment: CrossAxisAlignment.end,
                  ),
                ],
                alignment: MainAxisAlignment.spaceBetween,
              ),
              const SizedBox(height: 32),
              TextFormField(
                controller: c.fullName,
                decoration: const InputDecoration(
                  hintText: 'Full Name',
                ),
                validator: (v) {
                  if (v == null || v.isEmpty) {
                    return 'Full name required';
                  }
                },
              ),
              const SizedBox(height: 15),
              TextFormField(
                controller: c.email,
                decoration: const InputDecoration(
                  hintText: 'Email',
                ),
                validator: (v) {
                  if (v == null || v.isEmpty) {
                    return 'Email required';
                  }

                  if (!v.validateEmail()) {
                    return 'Email not valid';
                  }
                },
              ),
              const SizedBox(height: 15),
              TextFormField(
                controller: c.password,
                obscureText: true,
                obscuringCharacter: '*',
                decoration: const InputDecoration(
                  hintText: 'Password',
                ),
                validator: (v) {
                  if (v == null || v.isEmpty) {
                    return 'Password required';
                  }

                  if (v.length < 8) {
                    return 'Need at least 8 characters for password';
                  }
                },
              ),
              const SizedBox(height: 15),
              BlocBuilder<RegisterBloc, RegisterState>(
                builder: (context, state) {
                  return HStack([
                    Checkbox(
                      value: c.isAgree,
                      onChanged: (v) => c.add(
                        SetRegisterAgreement(v ?? false),
                      ),
                    ),
                    'By creating an account you agree to our Terms of Service and Privacy Policy'
                        .text
                        .make()
                        .expand(),
                  ]);
                },
              ),
              const SizedBox(height: 15),
              BlocConsumer<RegisterBloc, RegisterState>(
                listener: (context, state) {
                  if (state is RegisterError) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        content: Text(state.message),
                        backgroundColor: Colors.red,
                      ),
                    );
                  }

                  if (state is RegisterSuccess) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: Text('Register success'),
                        backgroundColor: Colors.green,
                      ),
                    );
                    context.read<AuthBloc>().add(CheckAuth());
                  }
                },
                builder: (context, state) {
                  if (state is RegisterLoading) {
                    return ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        fixedSize: const Size(300, 50),
                        textStyle: const TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                      ),
                      onPressed: null,
                      child: const CircularProgressIndicator().w(30).h(30),
                    );
                  }
                  return ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      fixedSize: const Size(300, 50),
                      textStyle: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                    ),
                    onPressed: () => c.add(SubmitRegister()),
                    child: const Text('Sign Up'),
                  );
                },
              ),
            ],
            crossAlignment: CrossAxisAlignment.stretch,
          ).hFull(context).wFull(context).px32().scrollVertical(),
        ),
      ),
    );
  }
}
