part of 'register_bloc.dart';

abstract class RegisterState extends Equatable {
  const RegisterState();
}

class RegisterInitial extends RegisterState {
  @override
  List<Object> get props => [];
}

class RegisterLoading extends RegisterState {
  @override
  List<Object> get props => [];
}

class RegisterSuccess extends RegisterState {
  @override
  List<Object> get props => [];
}

class RegisterError extends RegisterState {
  const RegisterError(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}

class RegisterLoaded extends RegisterState {
  @override
  List<Object> get props => [];
}
