part of 'register_bloc.dart';

abstract class RegisterEvent extends Equatable {
  const RegisterEvent();
}

class SubmitRegister extends RegisterEvent {
  @override
  List<Object?> get props => [];
}

class SetRegisterAgreement extends RegisterEvent {
  const SetRegisterAgreement(this.isAgree);

  final bool isAgree;

  @override
  List<Object?> get props => [isAgree];
}
