import 'package:alesha/domain/user/user.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

part 'register_event.dart';

part 'register_state.dart';

@injectable
class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  RegisterBloc(this._repo) : super(RegisterInitial()) {
    on<SubmitRegister>((event, emit) async {
      FocusScope.of(formKey.currentContext!).unfocus();
      if (formKey.currentState!.validate()) {
        if (!isFormSatisfied) {
          emit(const RegisterError('Please accept term and condition'));
        } else {
          emit(RegisterLoading());

          final register = await _repo.register(
            name: fullName.text,
            email: email.text,
            password: password.text,
          );

          register.fold(
            (l) => emit(RegisterError(l.message)),
            (r) => emit(RegisterSuccess()),
          );
        }
      }
    });

    on<SetRegisterAgreement>((event, emit) {
      emit(RegisterLoading());
      isAgree = event.isAgree;
      emit(RegisterLoaded());
    });
  }

  final UserRepo _repo;
  final formKey = GlobalKey<FormState>();
  final fullName = TextEditingController();
  final email = TextEditingController();
  final password = TextEditingController();
  bool isAgree = false;

  bool get isFormSatisfied =>
      fullName.text.isNotEmpty &&
      email.text.isNotEmpty &&
      password.text.isNotEmpty &&
      isAgree;
}
