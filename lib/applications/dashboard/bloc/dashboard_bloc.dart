import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'dashboard_event.dart';

part 'dashboard_state.dart';

class DashboardBloc extends Bloc<DashboardEvent, DashboardState> {
  DashboardBloc() : super(DashboardInitial()) {
    on<ChangeMenu>((event, emit) {
      emit(DashboardLoading());
      currentIndex = event.index;
      emit(DashboardLoaded());
    });
  }

  int currentIndex = 0;
}
