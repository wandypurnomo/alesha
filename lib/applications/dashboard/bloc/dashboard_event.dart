part of 'dashboard_bloc.dart';

abstract class DashboardEvent extends Equatable {
  const DashboardEvent();
}

class ChangeMenu extends DashboardEvent {
  const ChangeMenu(this.index);

  final int index;

  @override
  List<Object?> get props => [index];
}
