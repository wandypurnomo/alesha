import 'package:alesha/applications/app/app.dart';
import 'package:alesha/applications/dashboard/dashboard.dart';
import 'package:alesha/applications/login/login.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:velocity_x/velocity_x.dart';

class DashboardScreen extends StatelessWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => DashboardBloc(),
      child: const _DashboardScreen(),
    );
  }
}

class _DashboardScreen extends StatelessWidget {
  const _DashboardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthBloc, AuthState>(
      listener: (context, state) {
        if(state is LoggedOut){
          context.nextAndRemoveUntilPage(LoginScreen());
        }
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(70),
          child: BlocBuilder<DashboardBloc, DashboardState>(
            builder: (context, state) {
              final c = context.read<DashboardBloc>();
              if (c.currentIndex == 0) {
                return HStack([
                  const TextField(
                    readOnly: true,
                    decoration: InputDecoration(
                      hintText: 'Search doctor',
                    ),
                  ).expand(),
                ]);
              }

              if (c.currentIndex == 3) {
                return 'Profile'.text.size(22).center.bold.makeCentered();
              }
              return SizedBox();
            },
          )
              .box
              .padding(Vx.mLTRB(20, 0, 20, 20))
              .margin(Vx.mOnly(top: 30))
              .make(),
        ),
        bottomNavigationBar: BlocBuilder<DashboardBloc, DashboardState>(
          builder: (context, state) {
            final c = context.read<DashboardBloc>();
            return BottomNavigationBar(
              currentIndex: c.currentIndex,
              unselectedItemColor: Vx.gray300,
              selectedItemColor: Theme.of(context).colorScheme.primary,
              onTap: (i) => c.add(ChangeMenu(i)),
              items: [
                BottomNavigationBarItem(
                  icon: Icon(Icons.home),
                  label: 'Home',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.list_alt_rounded),
                  label: 'Appointment',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.email),
                  label: 'Inbox',
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.person),
                  label: 'Profile',
                ),
              ],
            );
          },
        ),
        body: BlocBuilder<DashboardBloc, DashboardState>(
          builder: (context, state) {
            final c = context.read<DashboardBloc>();
            return IndexedStack(
              index: c.currentIndex,
              children: [
                HomePage(),
                AppointmentPage(),
                InboxPage(),
                ProfilePage(),
              ],
            );
          },
        ),
      ),
    );
  }
}
