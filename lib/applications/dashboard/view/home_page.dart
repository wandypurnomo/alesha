import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return VStack(
      [
        'Top Doctor'.text.start.size(22).bold.make(),
        SizedBox(height: 10),
        HStack(
          [
            VStack(
              [
                Container(
                  width: 60,
                  height: 60,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: NetworkImage('https://via.placeholder.com/100'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(height: 5),
                'Ismail'.text.center.makeCentered(),
              ],
              crossAlignment: CrossAxisAlignment.stretch,
              alignment: MainAxisAlignment.center,
            ).box.rounded.outerShadow.white.make().aspectRatio(1).expand(),
            SizedBox(width: 20),
            VStack(
              [
                Container(
                  width: 60,
                  height: 60,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: NetworkImage('https://via.placeholder.com/100'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(height: 5),
                'Ismail'.text.center.makeCentered(),
              ],
              crossAlignment: CrossAxisAlignment.stretch,
              alignment: MainAxisAlignment.center,
            ).box.rounded.outerShadow.white.make().aspectRatio(1).expand(),
            SizedBox(width: 20),
            VStack(
              [
                Container(
                  width: 60,
                  height: 60,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: NetworkImage('https://via.placeholder.com/100'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(height: 5),
                'Ismail'.text.center.makeCentered(),
              ],
              crossAlignment: CrossAxisAlignment.stretch,
              alignment: MainAxisAlignment.center,
            ).box.rounded.outerShadow.white.make().aspectRatio(1).expand(),
          ],
          alignment: MainAxisAlignment.spaceBetween,
        ),
        SizedBox(height: 20),
        'My Appointment'.text.start.size(22).bold.make(),
        SizedBox(height: 10),
        HStack(
          [
            VStack(
              [
                Container(
                  width: 60,
                  height: 60,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: NetworkImage('https://via.placeholder.com/100'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(height: 5),
                'Ismail'.text.center.makeCentered(),
              ],
              crossAlignment: CrossAxisAlignment.stretch,
              alignment: MainAxisAlignment.center,
            ).box.rounded.outerShadow.white.make().aspectRatio(1).expand(),
            SizedBox(width: 20),
            VStack(
              [
                Container(
                  width: 60,
                  height: 60,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: NetworkImage('https://via.placeholder.com/100'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(height: 5),
                'Ismail'.text.center.makeCentered(),
              ],
              crossAlignment: CrossAxisAlignment.stretch,
              alignment: MainAxisAlignment.center,
            ).box.rounded.outerShadow.white.make().aspectRatio(1).expand(),
            SizedBox(width: 20),
            VStack(
              [
                Container(
                  width: 60,
                  height: 60,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: NetworkImage('https://via.placeholder.com/100'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(height: 5),
                'Ismail'.text.center.makeCentered(),
              ],
              crossAlignment: CrossAxisAlignment.stretch,
              alignment: MainAxisAlignment.center,
            ).box.rounded.outerShadow.white.make().aspectRatio(1).expand(),
          ],
          alignment: MainAxisAlignment.spaceBetween,
        ),
        SizedBox(height: 20),
        'Categories'.text.start.size(22).bold.make(),
        SizedBox(height: 10),
        HStack(
          [
            ...List.generate(
              20,
                  (index) => HStack(
                [
                  Container(
                    width: 20,
                    height: 20,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image:
                        NetworkImage('https://via.placeholder.com/100'),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  SizedBox(width: 5),
                  'Category $index'.text.center.makeCentered(),
                ],
                crossAlignment: CrossAxisAlignment.stretch,
                alignment: MainAxisAlignment.center,
              )
                  .box
                  .rounded
                  .outerShadow
                  .white
                  .width(100)
                  .height(40)
                  .margin(Vx.mLTRB(2, 2, 10, 2))
                  .make(),
            ).toList()
          ],
          alignment: MainAxisAlignment.spaceBetween,
        ).scrollHorizontal(),
      ],
      crossAlignment: CrossAxisAlignment.stretch,
    ).px20();
  }
}
