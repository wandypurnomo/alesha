import 'package:alesha/applications/app/app.dart';
import 'package:alesha/applications/update_profile/update_profile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:velocity_x/velocity_x.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return BlocBuilder<AuthBloc, AuthState>(
      builder: (context, state) {
        final c = context.read<AuthBloc>().user;
        return VStack(
          [
            Container(
              width: 100,
              height: 100,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: NetworkImage(c!.photo!),
                  fit: BoxFit.contain,
                ),
              ),
            ),
            const SizedBox(height: 20),
            'Name'.text.size(16).bold.make(),
            c.name.text.size(16).make(),
            const SizedBox(height: 10),
            'Email'.text.size(16).bold.make(),
            c.email.text.size(16).make(),
            SizedBox(height: 40),
            ElevatedButton(
                onPressed: () =>
                    context.push<dynamic>((context) => UpdateProfileScreen()),
                child: Text('Edit Profile')),
            ElevatedButton(
              onPressed: () => context.read<AuthBloc>().add(Logout()),
              child: Text('Logout'),
              style: ElevatedButton.styleFrom(
                primary: Colors.red,
              ),
            ),
          ],
          crossAlignment: CrossAxisAlignment.stretch,
        );
      },
    ).p20();
  }
}
