import 'package:alesha/applications/app/app.dart';
import 'package:alesha/applications/update_profile/update_profile.dart';
import 'package:alesha/infrastructure/helper/helper.dart';
import 'package:alesha/injection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:velocity_x/velocity_x.dart';

class UpdateProfileScreen extends StatelessWidget {
  const UpdateProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<UpdateProfileBloc>(),
      child: const _UpdateProfileScreen(),
    );
  }
}

class _UpdateProfileScreen extends StatelessWidget {
  const _UpdateProfileScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Update Profile'),
        centerTitle: true,
      ),
      body: BlocListener<AuthBloc, AuthState>(
        listener: (context, state) {
          if (state is AuthSuccess) {
            ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                content: Text('Profile Updated'),
                backgroundColor: Colors.green,
              ),
            );
            context.pop<dynamic>();
          }
        },
        child: BlocBuilder<UpdateProfileBloc, UpdateProfileState>(
          builder: (context, state) {
            final upb = context.read<UpdateProfileBloc>();
            final ab = context.read<AuthBloc>();
            final ub = context.read<UploadBloc>();
            upb.name.text = ab.user!.name;
            upb.email.text = ab.user!.email;
            if (upb.selectedFile == null) {
              upb.placeholder = NetworkImage(
                  ab.user!.photo ?? 'https://via.placeholder.com/100');
            }

            return VStack(
              [
                ZStack([
                  Align(
                    child: Container(
                      width: 100,
                      height: 100,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          image: upb.placeholder!,
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                  ),
                  Align(
                    child: 'Tap to edit'
                        .text
                        .center
                        .white
                        .makeCentered()
                        .box
                        .color(Vx.black.withOpacity(0.4))
                        .width(100)
                        .height(100)
                        .roundedFull
                        .make(),
                  ),
                ]).onTap(() async {
                  final im = await MediaProcess.getImageFromCamera(crop: true);
                  if (im != null) {
                    upb.add(SetImageFile(im));
                    ub.selectedFile = im;
                  }
                }),
                const SizedBox(height: 15),
                TextFormField(
                  controller: upb.name,
                  decoration: const InputDecoration(
                    hintText: 'Full Name',
                  ),
                  validator: (v) {
                    if (v == null || v.isEmpty) {
                      return 'Full name required';
                    }
                  },
                ),
                const SizedBox(height: 15),
                TextFormField(
                  controller: upb.email,
                  decoration: const InputDecoration(
                    hintText: 'Email',
                  ),
                  validator: (v) {
                    if (v == null || v.isEmpty) {
                      return 'Email required';
                    }

                    if (!v.validateEmail()) {
                      return 'Email not valid';
                    }
                  },
                ),
                const SizedBox(height: 15),
                BlocConsumer<UploadBloc, UploadState>(
                  listener: (context, state) {
                    if (state is UploadSuccess) {
                      upb.profilePhotoUrl = state.url;
                      upb.add(SubmitUpdateProfile());
                    }
                  },
                  builder: (context, state) {
                    if (state is UploadLoading) {
                      return ElevatedButton(
                        onPressed: null,
                        child: const CircularProgressIndicator().h(20).w(20),
                        style: ElevatedButton.styleFrom(
                          fixedSize: const Size(300, 50),
                          textStyle: const TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30),
                          ),
                        ),
                      );
                    }
                    return BlocConsumer<UpdateProfileBloc, UpdateProfileState>(
                      listener: (context, state) {
                        if (state is UpdateProfileSuccess) {
                          ab.add(CheckAuth());
                        }
                      },
                      builder: (context, state) {
                        if (state is UpdateProfileLoading) {
                          return ElevatedButton(
                            onPressed: null,
                            child:
                                const CircularProgressIndicator().h(20).w(20),
                            style: ElevatedButton.styleFrom(
                              fixedSize: const Size(300, 50),
                              textStyle: const TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30),
                              ),
                            ),
                          );
                        }
                        return ElevatedButton(
                          onPressed: () {
                            if (upb.selectedFile == null) {
                              ub.add(BayPassUploadFile());
                            } else {
                              ub.add(UploadFile());
                            }
                          },
                          style: ElevatedButton.styleFrom(
                            fixedSize: const Size(300, 50),
                            textStyle: const TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30),
                            ),
                          ),
                          child: const Text('Save'),
                        );
                      },
                    );
                  },
                ),
              ],
              crossAlignment: CrossAxisAlignment.stretch,
            );
          },
        ),
      ).p20().scrollVertical(),
    );
  }
}
