part of 'update_profile_bloc.dart';

abstract class UpdateProfileState extends Equatable {
  const UpdateProfileState();
}

class UpdateProfileInitial extends UpdateProfileState {
  @override
  List<Object> get props => [];
}

class UpdateProfileLoading extends UpdateProfileState {
  @override
  List<Object> get props => [];
}

class UpdateProfileLoaded extends UpdateProfileState {
  @override
  List<Object> get props => [];
}

class UpdateProfileSuccess extends UpdateProfileState {
  const UpdateProfileSuccess(this.user);

  final User user;

  @override
  List<Object> get props => [user];
}

class UpdateProfileError extends UpdateProfileState {
  const UpdateProfileError(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}
