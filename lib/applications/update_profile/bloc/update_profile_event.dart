part of 'update_profile_bloc.dart';

abstract class UpdateProfileEvent extends Equatable {
  const UpdateProfileEvent();
}

class SubmitUpdateProfile extends UpdateProfileEvent {
  @override
  List<Object?> get props => [];
}

class SetPhotoUrl extends UpdateProfileEvent {
  const SetPhotoUrl(this.url);

  final String url;

  @override
  List<Object?> get props => [url];
}

class SetImageFile extends UpdateProfileEvent {
  const SetImageFile(this.file);

  final File? file;

  @override
  List<Object?> get props => [file];
}
