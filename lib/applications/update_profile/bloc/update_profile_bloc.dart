import 'dart:io';

import 'package:alesha/domain/user/user.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

part 'update_profile_event.dart';

part 'update_profile_state.dart';

@injectable
class UpdateProfileBloc extends Bloc<UpdateProfileEvent, UpdateProfileState> {
  UpdateProfileBloc(this._repo) : super(UpdateProfileInitial()) {
    on<SubmitUpdateProfile>((event, emit) async {
      emit(UpdateProfileLoading());
      final nameField = name.text.isEmpty ? null : name.text;
      final emailField = email.text.isEmpty ? null : email.text;
      final photoUrlField = profilePhotoUrl.isEmpty ? null : profilePhotoUrl;

      final update = await _repo.updateProfile(
        name: nameField,
        email: emailField,
        photo: photoUrlField,
      );

      update.fold(
        (l) => emit(UpdateProfileError(l.message)),
        (r) => emit(UpdateProfileSuccess(r)),
      );
    });

    on<SetPhotoUrl>((event, emit) {
      emit(UpdateProfileLoading());
      profilePhotoUrl = event.url;
      emit(UpdateProfileLoaded());
    });

    on<SetImageFile>((event, emit) {
      emit(UpdateProfileLoading());
      selectedFile = event.file;
      placeholder = FileImage(event.file!);
      emit(UpdateProfileLoaded());
    });
  }

  final UserRepo _repo;
  final formKey = GlobalKey<FormState>();
  final name = TextEditingController();
  final email = TextEditingController();
  ImageProvider? placeholder;
  File? selectedFile;
  String profilePhotoUrl = '';
}
