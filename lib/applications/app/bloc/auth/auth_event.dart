part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();
}

class CheckAuth extends AuthEvent {
  @override
  List<Object?> get props => [];
}

class Logout extends AuthEvent {
  @override
  List<Object?> get props => [];
}

class SetUser extends AuthEvent {
  const SetUser(this.user);

  final User user;

  @override
  List<Object?> get props => [user];
}
