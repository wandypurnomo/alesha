part of 'auth_bloc.dart';

abstract class AuthState extends Equatable {
  const AuthState();
}

class AuthInitial extends AuthState {
  @override
  List<Object> get props => [];
}

class AuthLoading extends AuthState {
  @override
  List<Object> get props => [];
}

class LoggedOut extends AuthState {
  @override
  List<Object> get props => [];
}

class AuthSuccess extends AuthState {
  const AuthSuccess(this.user);

  final User user;

  @override
  List<Object> get props => [user];
}

class AuthFailed extends AuthState {
  const AuthFailed(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}
