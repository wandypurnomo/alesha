import 'package:alesha/domain/user/user.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';

part 'auth_event.dart';

part 'auth_state.dart';

@injectable
class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc(this._repo) : super(AuthInitial()) {
    on<CheckAuth>((event, emit) async {
      emit(AuthLoading());
      final check = await _repo.profile();
      check.fold(
        (l) => emit(AuthFailed(l.message)),
        (r) {
          _user = r;
          emit(AuthSuccess(r));
        },
      );
    });

    on<SetUser>((event, emit) async {
      emit(AuthLoading());
      _user = event.user;
      emit(AuthSuccess(event.user));
    });

    on<Logout>((event, emit) async {
      emit(AuthLoading());
      final check = await _repo.logout();
      check.fold(
        (l) => emit(AuthFailed(l.message)),
        (r) {
          _user = null;
          emit(LoggedOut());
        },
      );
    });
  }

  final UserRepo _repo;
  User? _user;

  User? get user => _user;
}
