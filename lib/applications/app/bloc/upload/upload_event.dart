part of 'upload_bloc.dart';

abstract class UploadEvent extends Equatable {
  const UploadEvent();
}

class UploadFile extends UploadEvent {
  @override
  List<Object?> get props => [];
}

class BayPassUploadFile extends UploadEvent {
  @override
  List<Object?> get props => [];
}
