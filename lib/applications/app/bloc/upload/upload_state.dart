part of 'upload_bloc.dart';

abstract class UploadState extends Equatable {
  const UploadState();
}

class UploadInitial extends UploadState {
  @override
  List<Object> get props => [];
}

class UploadLoading extends UploadState {
  @override
  List<Object> get props => [];
}

class UploadSuccess extends UploadState {
  const UploadSuccess(this.url);

  final String url;

  @override
  List<Object> get props => [url];
}

class UploadError extends UploadState {
  const UploadError(this.message);

  final String message;

  @override
  List<Object> get props => [message];
}
