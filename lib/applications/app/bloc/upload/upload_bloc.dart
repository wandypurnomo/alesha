import 'dart:io';

import 'package:alesha/infrastructure/helper/helper.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'upload_event.dart';

part 'upload_state.dart';

class UploadBloc extends Bloc<UploadEvent, UploadState> {
  UploadBloc() : super(UploadInitial()) {
    on<UploadFile>((event, emit) async {
      emit(UploadLoading());
      if (selectedFile == null) {
        emit(const UploadError('No file selected'));
      } else {
        final u = await upload(selectedFile!);
        if (u.isEmpty) {
          emit(const UploadError('Upload failed'));
        } else {
          emit(UploadSuccess(u));
        }
      }
    });

    on<BayPassUploadFile>((event, emit) {
      emit(UploadLoading());
      emit(const UploadSuccess(''));
    });
  }

  File? selectedFile;
}
