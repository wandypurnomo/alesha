import 'package:alesha/applications/app/app.dart';
import 'package:alesha/injection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RootScreen extends StatelessWidget {
  const RootScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => getIt<AuthBloc>()..add(CheckAuth())),
        BlocProvider(create: (context) => UploadBloc()),
      ],
      child: const App(),
    );
  }
}
