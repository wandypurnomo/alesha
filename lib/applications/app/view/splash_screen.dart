import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: VStack(
        [
          Image.asset(
            'assets/logo.png',
            width: 130,
          ).box.makeCentered(),
          const SizedBox(height: 10),
          'Alesha'.text.white.wide.center.size(50).bold.make(),
        ],
        crossAlignment: CrossAxisAlignment.stretch,
        alignment: MainAxisAlignment.center,
        axisSize: MainAxisSize.max,
      )
          .box
          .gradientFromTo(
            from: const Color(0xFF4A80FF),
            to: const Color(0xFF3462FF),
          )
          .make(),
    );
  }
}
