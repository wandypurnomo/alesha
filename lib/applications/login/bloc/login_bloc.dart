import 'package:alesha/domain/user/user.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

part 'login_event.dart';

part 'login_state.dart';

@injectable
class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc(this._repo) : super(LoginInitial()) {
    on<TraditionalLogin>((event, emit) async {
      if (formKey.currentState!.validate()) {
        FocusScope.of(formKey.currentContext!).unfocus();
        emit(LoginLoading());

        final login = await _repo.traditionalLogin(
          email.text,
          password.text,
        );

        login.fold(
          (l) => emit(LoginError(l.message)),
          (r) => emit(LoginSuccess()),
        );
      }
    });

    on<GoogleLogin>((event, emit) async {
      emit(LoginLoading());

      final login = await _repo.googleLogin();

      login.fold(
        (l) => emit(LoginError(l.message)),
        (r) => emit(LoginSuccess()),
      );
    });
  }

  final UserRepo _repo;
  final formKey = GlobalKey<FormState>();
  final email = TextEditingController();
  final password = TextEditingController();
}
