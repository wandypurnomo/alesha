part of 'login_bloc.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();
}

class TraditionalLogin extends LoginEvent {
  @override
  List<Object?> get props => [];
}

class GoogleLogin extends LoginEvent {
  @override
  List<Object?> get props => [];
}
