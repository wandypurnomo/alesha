import 'package:alesha/applications/app/app.dart';
import 'package:alesha/applications/dashboard/dashboard.dart';
import 'package:alesha/applications/login/bloc/login_bloc.dart';
import 'package:alesha/applications/register/register.dart';
import 'package:alesha/injection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:velocity_x/velocity_x.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<LoginBloc>(),
      child: const _LoginScreen(),
    );
  }
}

class _LoginScreen extends StatelessWidget {
  const _LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final c = context.read<LoginBloc>();
    return Scaffold(
      body: MultiBlocListener(
        listeners: [
          BlocListener<AuthBloc, AuthState>(
            listener: (context, state) {
              if (state is AuthSuccess) {
                context.nextAndRemoveUntilPage(const DashboardScreen());
              }
            },
          ),
          BlocListener<LoginBloc, LoginState>(
            listener: (context, state) {
              if (state is LoginError) {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    content: Text(state.message),
                    backgroundColor: Colors.red,
                  ),
                );
              }

              if (state is LoginSuccess) {
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                    content: Text('Login success'),
                    backgroundColor: Colors.green,
                  ),
                );
                context.read<AuthBloc>().add(CheckAuth());
              }
            },
          ),
        ],
        child: Form(
          key: c.formKey,
          child: VStack(
            [
              Image.asset(
                'assets/logo_blue.png',
                width: 130,
              ).box.makeCentered(),
              const SizedBox(height: 10),
              'Alesha'
                  .text
                  .color(const Color(0xFF4A80FF))
                  .wide
                  .center
                  .size(50)
                  .bold
                  .make(),
              const SizedBox(height: 40),
              'Welcome Back'.text.size(22).bold.start.make(),
              'sign in to continue'
                  .text
                  .size(20)
                  .color(const Color(0xFF8A8D9F))
                  .make(),
              const SizedBox(height: 40),
              TextFormField(
                controller: c.email,
                decoration: const InputDecoration(
                  hintText: 'Email',
                  prefixIcon: Icon(
                    Icons.email,
                    color: Color(0xFF4A80FF),
                  ),
                ),
                validator: (v) {
                  if (v == null || v.isEmpty) {
                    return 'Email required';
                  }

                  if (!v.validateEmail()) {
                    return 'Email not valid';
                  }
                },
              ),
              const SizedBox(height: 15),
              TextFormField(
                controller: c.password,
                obscureText: true,
                obscuringCharacter: '*',
                decoration: const InputDecoration(
                  hintText: 'Password',
                  prefixIcon: Icon(
                    Icons.lock,
                    color: Color(0xFF4A80FF),
                  ),
                ),
                validator: (v) {
                  if (v == null || v.isEmpty) {
                    return 'Password required';
                  }
                },
              ),
              const SizedBox(height: 15),
              'Forgot password ?'
                  .text
                  .color(const Color(0xFF4A80FF))
                  .size(16)
                  .end
                  .make(),
              const SizedBox(height: 15),
              BlocBuilder<LoginBloc, LoginState>(
                builder: (context, state) {
                  if (state is LoginLoading) {
                    return ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        fixedSize: const Size(300, 50),
                        textStyle: const TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                      ),
                      onPressed: null,
                      child: const CircularProgressIndicator().w(30).h(30),
                    );
                  }
                  return ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      fixedSize: const Size(300, 50),
                      textStyle: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                    ),
                    onPressed: () => c.add(TraditionalLogin()),
                    child: const Text('Sign In'),
                  );
                },
              ),
              const SizedBox(height: 15),
              '/'.text.center.size(16).color(const Color(0xFF8A8D9F)).make(),
              const SizedBox(height: 15),
              HStack(
                [
                  Image.asset(
                    'assets/google_icon.png',
                    height: 18,
                  )
                      .box
                      .p16
                      .rounded
                      .border(color: const Color(0xFF8A8D9F))
                      .make()
                      .onTap(() {
                    c.add(GoogleLogin());
                  }),
                  Image.asset(
                    'assets/appple_icon.png',
                    height: 18,
                  )
                      .box
                      .p16
                      .rounded
                      .border(color: const Color(0xFF8A8D9F))
                      .make(),
                  Image.asset(
                    'assets/facebook_icon.png',
                    height: 18,
                  )
                      .box
                      .p16
                      .rounded
                      .border(color: const Color(0xFF8A8D9F))
                      .make(),
                ],
                alignment: MainAxisAlignment.spaceEvenly,
              ),
              const SizedBox(height: 60),
              HStack(
                [
                  'Dont have account? '.text.size(16).make(),
                  InkWell(
                    onTap: () => context.push<dynamic>(
                      (context) => const RegisterScreen(),
                    ),
                    child: 'Sign Up!'
                        .text
                        .size(16)
                        .bold
                        .color(const Color(0xFF4A80FF))
                        .make(),
                  ),
                ],
                alignment: MainAxisAlignment.center,
              ),
            ],
            crossAlignment: CrossAxisAlignment.stretch,
            alignment: MainAxisAlignment.center,
          ).hFull(context).wFull(context).px32().scrollVertical(),
        ),
      ),
    );
  }
}
