part of 'doctor_list_bloc.dart';

abstract class DoctorListState extends Equatable {
  const DoctorListState();
}

class DoctorListInitial extends DoctorListState {
  @override
  List<Object> get props => [];
}

class DoctorListLoading extends DoctorListState {
  @override
  List<Object> get props => [];
}

class DoctorListLoaded extends DoctorListState {
  const DoctorListLoaded(this.doctors);

  final List<Doctor> doctors;

  @override
  List<Object> get props => [doctors];
}
