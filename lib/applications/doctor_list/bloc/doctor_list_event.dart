part of 'doctor_list_bloc.dart';

abstract class DoctorListEvent extends Equatable {
  const DoctorListEvent();
}

class GetDoctors extends DoctorListEvent {
  @override
  List<Object?> get props => [];
}

class SearchDoctors extends DoctorListEvent {
  const SearchDoctors(this.keyword);

  final String keyword;

  @override
  List<Object?> get props => [keyword];
}
