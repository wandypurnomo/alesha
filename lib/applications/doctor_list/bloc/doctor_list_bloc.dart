import 'package:alesha/domain/user/user.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';

part 'doctor_list_event.dart';

part 'doctor_list_state.dart';

@injectable
class DoctorListBloc extends Bloc<DoctorListEvent, DoctorListState> {
  DoctorListBloc(this._repo) : super(DoctorListInitial()) {
    on<GetDoctors>((event, emit) async {
      emit(DoctorListLoading());
      final doctors = await _repo.doctors();
      doctors.fold(
        (l) => emit(const DoctorListLoaded([])),
        (r) {
          _allDoctors
            ..clear()
            ..addAll(r);
          emit(DoctorListLoaded(_allDoctors));
        },
      );
    });

    on<SearchDoctors>((event, emit) {
      emit(DoctorListLoading());
      final search = _allDoctors
          .where(
            (e) => e.name.toLowerCase().contains(
                  event.keyword.toLowerCase(),
                ),
          )
          .toList();
      emit(DoctorListLoaded(search));
    });
  }

  final UserRepo _repo;
  final _allDoctors = <Doctor>[];
}
