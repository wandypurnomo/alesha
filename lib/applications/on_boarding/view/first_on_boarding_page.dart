import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';

class FirstOnBoardingPage extends StatelessWidget {
  const FirstOnBoardingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return VStack(
      [
        VStack(
          [
            'Welcome to Alesha'.text.size(30).bold.center.make(),
            const SizedBox(height: 22),
            'Reference site about Lorem \n'
                    'Ipsum, giving information origins \n'
                    'as well as a random'
                .text
                .size(16)
                .heightRelaxed
                .center
                .make()
                .px(80),
          ],
          axisSize: MainAxisSize.max,
          crossAlignment: CrossAxisAlignment.center,
          alignment: MainAxisAlignment.center,
        ).expand(),
        ZStack([
          Positioned(
            left: 20,
            bottom: 20,
            child: Image.asset(
              'assets/fig_1.png',
              height: 250,
            ),
          ),
        ]).expand(),
      ],
      crossAlignment: CrossAxisAlignment.stretch,
    ).box.make();
  }
}
