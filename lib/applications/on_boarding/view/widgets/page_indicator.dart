import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';

class PageIndicator extends StatelessWidget {
  const PageIndicator({
    Key? key,
    this.currentIndex = 0,
    required this.length,
  }) : super(key: key);

  final int currentIndex;
  final int length;

  @override
  Widget build(BuildContext context) {
    final widgets = List.generate(length, (index) {
      if (index == currentIndex) {
        return const ActiveIndicator().px(6);
      }
      return const InactiveIndicator().px(6);
    }).toList();

    return HStack(widgets);
  }
}

class ActiveIndicator extends StatelessWidget {
  const ActiveIndicator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 12,
      height: 12,
      decoration: BoxDecoration(
        color: Vx.white.withOpacity(0.5),
        shape: BoxShape.circle,
      ),
    );
  }
}

class InactiveIndicator extends StatelessWidget {
  const InactiveIndicator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 12,
      height: 12,
      decoration: const BoxDecoration(
        color: Vx.white,
        shape: BoxShape.circle,
      ),
    );
  }
}
