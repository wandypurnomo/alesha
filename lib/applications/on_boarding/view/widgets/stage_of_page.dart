import 'package:flutter/material.dart';
import 'package:velocity_x/velocity_x.dart';

class StageOfPage extends StatelessWidget {
  const StageOfPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return VStack(
      [
        VxBox().white.bottomRounded(value: 40).make().expand(),
        ZStack([
          Positioned(
            bottom: 0,
            child: Image.asset(
              'assets/ellipse.png',
              width: 283,
            ),
          ),
          Positioned(
            right: 0,
            bottom: 0,
            child: Image.asset(
              'assets/ellipse_2.png',
              height: 283,
            ),
          ),
        ]).expand(),
      ],
      crossAlignment: CrossAxisAlignment.stretch,
    )
        .box
        .gradientFromTo(
          from: const Color(0xFF4A80FF),
          to: const Color(0xFF3462FF),
        )
        .make();
  }
}
