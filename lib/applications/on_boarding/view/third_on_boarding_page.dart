import 'package:alesha/applications/login/login.dart';
import 'package:alesha/applications/on_boarding/on_boarding.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:velocity_x/velocity_x.dart';

class ThirdOnBoardingPage extends StatelessWidget {
  const ThirdOnBoardingPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return VStack(
      [
        ZStack(
          [
            VStack(
              [
                'Get Fitness Trips'.text.size(30).bold.center.make(),
                const SizedBox(height: 22),
                'Reference site about Lorem \n'
                        'Ipsum, giving information origins \n'
                        'as well as a random'
                    .text
                    .size(16)
                    .heightRelaxed
                    .center
                    .make()
                    .px(80),
              ],
              axisSize: MainAxisSize.max,
              crossAlignment: CrossAxisAlignment.center,
              alignment: MainAxisAlignment.center,
            ),
            Positioned(
              bottom: 30,
              left: 30,
              right: 30,
              child: BlocBuilder<OnBoardingCubit, OnBoardingState>(
                builder: (context, state) {
                  final c = context.read<OnBoardingCubit>();
                  return AnimatedOpacity(
                    opacity: c.currentIndex == 2 ? 1 : 0,
                    duration: const Duration(milliseconds: 700),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        fixedSize: const Size(300, 50),
                        textStyle: const TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                      ),
                      onPressed: () {
                        context.nextAndRemoveUntilPage(const LoginScreen());
                      },
                      child: const Text('Continue'),
                    ),
                  );
                },
              ),
            ),
          ],
          fit: StackFit.expand,
        ).expand(),
        ZStack([
          Positioned(
            left: 20,
            bottom: 20,
            child: Image.asset(
              'assets/fig_3.png',
              height: 283,
            ),
          ),
        ]).expand(),
      ],
      crossAlignment: CrossAxisAlignment.stretch,
    ).box.make();
  }
}
