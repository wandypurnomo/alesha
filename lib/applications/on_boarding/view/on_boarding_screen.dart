import 'package:alesha/applications/on_boarding/on_boarding.dart';
import 'package:alesha/applications/on_boarding/view/first_on_boarding_page.dart';
import 'package:alesha/applications/on_boarding/view/second_on_boarding_page.dart';
import 'package:alesha/applications/on_boarding/view/third_on_boarding_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:velocity_x/velocity_x.dart';

import 'widgets/page_indicator.dart';
import 'widgets/stage_of_page.dart';

class OnBoardingScreen extends StatelessWidget {
  const OnBoardingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => OnBoardingCubit(),
      child: const _OnBoardingScreen(),
    );
  }
}

class _OnBoardingScreen extends StatelessWidget {
  const _OnBoardingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ZStack(
        [
          const StageOfPage(),
          PageView(
            onPageChanged: context.read<OnBoardingCubit>().setIndex,
            children: const [
              FirstOnBoardingPage(),
              SecondOnBoardingPage(),
              ThirdOnBoardingPage(),
            ],
          ),
          Positioned(
            bottom: 20,
            right: 20,
            child: BlocBuilder<OnBoardingCubit, OnBoardingState>(
              builder: (context, state) {
                return PageIndicator(
                  length: 3,
                  currentIndex: context.read<OnBoardingCubit>().currentIndex,
                );
              },
            ),
          ),
        ],
        fit: StackFit.expand,
      ),
    );
  }
}
