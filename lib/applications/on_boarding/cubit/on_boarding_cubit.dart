import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'on_boarding_state.dart';

class OnBoardingCubit extends Cubit<OnBoardingState> {
  OnBoardingCubit() : super(OnBoardingInitial());

  final pageController = PageController();
  int currentIndex = 0;

  void setIndex(int i){
    emit(OnBoardingLoading());
    currentIndex = i;
    emit(OnBoardingLoaded());
  }
}
