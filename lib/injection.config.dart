// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import 'applications/app/bloc/auth/auth_bloc.dart' as _i5;
import 'applications/doctor_list/bloc/doctor_list_bloc.dart' as _i6;
import 'applications/login/bloc/login_bloc.dart' as _i7;
import 'applications/register/bloc/register_bloc.dart' as _i8;
import 'applications/update_profile/bloc/update_profile_bloc.dart' as _i9;
import 'domain/user/user.dart' as _i3;
import 'infrastructure/impl/user_impl.dart' as _i4;

const String _development = 'development';
const String _production = 'production';
const String _staging = 'staging';
// ignore_for_file: unnecessary_lambdas
// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  gh.factory<_i3.UserRepo>(() => _i4.UserImpl(),
      registerFor: {_development, _production, _staging});
  gh.factory<_i5.AuthBloc>(() => _i5.AuthBloc(get<_i3.UserRepo>()));
  gh.factory<_i6.DoctorListBloc>(() => _i6.DoctorListBloc(get<_i3.UserRepo>()));
  gh.factory<_i7.LoginBloc>(() => _i7.LoginBloc(get<_i3.UserRepo>()));
  gh.factory<_i8.RegisterBloc>(() => _i8.RegisterBloc(get<_i3.UserRepo>()));
  gh.factory<_i9.UpdateProfileBloc>(
      () => _i9.UpdateProfileBloc(get<_i3.UserRepo>()));
  return get;
}
