import 'package:dartz/dartz.dart';

import '../exception/user_exception.dart';
import '../models/appointment.dart';
import '../models/doctor.dart';
import '../models/user.dart';

abstract class UserRepo {
  Future<Either<UserException, void>> traditionalLogin(
    String email,
    String password,
  );

  Future<Either<UserException, void>> googleLogin();

  Future<Either<UserException, void>> logout();

  Future<Either<UserException, User>> profile();

  Future<Either<UserException, User>> updateProfile({
    String? name,
    String? email,
    String? photo,
  });

  Future<Either<UserException, List<Appointment>>> myAppointment();

  Future<Either<UserException, List<Doctor>>> doctors({String name = ''});

  Future<Either<UserException, void>> register({
    required String name,
    required String email,
    required String password,
  });
}
