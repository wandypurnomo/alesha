import 'package:freezed_annotation/freezed_annotation.dart';

part 'doctor.freezed.dart';

part 'doctor.g.dart';

@freezed
class Doctor with _$Doctor {
  const factory Doctor({
    @Default('') String id,
    @Default('') String name,
    @Default('') String email,
    @Default('https://via.placeholder.com/100') String photo,
    @Default('') String location,
  }) = _Doctor;

  factory Doctor.fromJson(Map<String, dynamic> json) => _$DoctorFromJson(json);
}
