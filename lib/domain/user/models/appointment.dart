import 'package:alesha/domain/user/models/doctor.dart';
import 'package:alesha/domain/user/models/user.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'appointment.freezed.dart';

part 'appointment.g.dart';

@freezed
class Appointment with _$Appointment {
  const factory Appointment({
    required String id,
    required User user,
    required Doctor doctor,
    required DateTime dateTime,
  }) = _Appointment;

  factory Appointment.fromJson(Map<String, dynamic> json) =>
      _$AppointmentFromJson(json);
}
