import 'package:firebase_auth/firebase_auth.dart' as fa;
import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.freezed.dart';

part 'user.g.dart';

@freezed
class User with _$User {
  const factory User({
    @Default('') String id,
    @Default('') String name,
    @Default('') String email,
    @Default('https://via.placeholder.com/100') String? photo,
  }) = _User;

  factory User.fromFirebaseUser(fa.User firebaseUser) {
    return User(
      id: firebaseUser.uid,
      name: firebaseUser.displayName!,
      email: firebaseUser.email!,
      photo: firebaseUser.photoURL,
    );
  }

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
}
