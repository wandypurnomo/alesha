// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'doctor.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Doctor _$DoctorFromJson(Map<String, dynamic> json) {
  return _Doctor.fromJson(json);
}

/// @nodoc
class _$DoctorTearOff {
  const _$DoctorTearOff();

  _Doctor call(
      {String id = '',
      String name = '',
      String email = '',
      String photo = 'https://via.placeholder.com/100',
      String location = ''}) {
    return _Doctor(
      id: id,
      name: name,
      email: email,
      photo: photo,
      location: location,
    );
  }

  Doctor fromJson(Map<String, Object?> json) {
    return Doctor.fromJson(json);
  }
}

/// @nodoc
const $Doctor = _$DoctorTearOff();

/// @nodoc
mixin _$Doctor {
  String get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get email => throw _privateConstructorUsedError;
  String get photo => throw _privateConstructorUsedError;
  String get location => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DoctorCopyWith<Doctor> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DoctorCopyWith<$Res> {
  factory $DoctorCopyWith(Doctor value, $Res Function(Doctor) then) =
      _$DoctorCopyWithImpl<$Res>;
  $Res call(
      {String id, String name, String email, String photo, String location});
}

/// @nodoc
class _$DoctorCopyWithImpl<$Res> implements $DoctorCopyWith<$Res> {
  _$DoctorCopyWithImpl(this._value, this._then);

  final Doctor _value;
  // ignore: unused_field
  final $Res Function(Doctor) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? email = freezed,
    Object? photo = freezed,
    Object? location = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      photo: photo == freezed
          ? _value.photo
          : photo // ignore: cast_nullable_to_non_nullable
              as String,
      location: location == freezed
          ? _value.location
          : location // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$DoctorCopyWith<$Res> implements $DoctorCopyWith<$Res> {
  factory _$DoctorCopyWith(_Doctor value, $Res Function(_Doctor) then) =
      __$DoctorCopyWithImpl<$Res>;
  @override
  $Res call(
      {String id, String name, String email, String photo, String location});
}

/// @nodoc
class __$DoctorCopyWithImpl<$Res> extends _$DoctorCopyWithImpl<$Res>
    implements _$DoctorCopyWith<$Res> {
  __$DoctorCopyWithImpl(_Doctor _value, $Res Function(_Doctor) _then)
      : super(_value, (v) => _then(v as _Doctor));

  @override
  _Doctor get _value => super._value as _Doctor;

  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? email = freezed,
    Object? photo = freezed,
    Object? location = freezed,
  }) {
    return _then(_Doctor(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      photo: photo == freezed
          ? _value.photo
          : photo // ignore: cast_nullable_to_non_nullable
              as String,
      location: location == freezed
          ? _value.location
          : location // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Doctor implements _Doctor {
  const _$_Doctor(
      {this.id = '',
      this.name = '',
      this.email = '',
      this.photo = 'https://via.placeholder.com/100',
      this.location = ''});

  factory _$_Doctor.fromJson(Map<String, dynamic> json) =>
      _$$_DoctorFromJson(json);

  @JsonKey()
  @override
  final String id;
  @JsonKey()
  @override
  final String name;
  @JsonKey()
  @override
  final String email;
  @JsonKey()
  @override
  final String photo;
  @JsonKey()
  @override
  final String location;

  @override
  String toString() {
    return 'Doctor(id: $id, name: $name, email: $email, photo: $photo, location: $location)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _Doctor &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.name, name) &&
            const DeepCollectionEquality().equals(other.email, email) &&
            const DeepCollectionEquality().equals(other.photo, photo) &&
            const DeepCollectionEquality().equals(other.location, location));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(name),
      const DeepCollectionEquality().hash(email),
      const DeepCollectionEquality().hash(photo),
      const DeepCollectionEquality().hash(location));

  @JsonKey(ignore: true)
  @override
  _$DoctorCopyWith<_Doctor> get copyWith =>
      __$DoctorCopyWithImpl<_Doctor>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_DoctorToJson(this);
  }
}

abstract class _Doctor implements Doctor {
  const factory _Doctor(
      {String id,
      String name,
      String email,
      String photo,
      String location}) = _$_Doctor;

  factory _Doctor.fromJson(Map<String, dynamic> json) = _$_Doctor.fromJson;

  @override
  String get id;
  @override
  String get name;
  @override
  String get email;
  @override
  String get photo;
  @override
  String get location;
  @override
  @JsonKey(ignore: true)
  _$DoctorCopyWith<_Doctor> get copyWith => throw _privateConstructorUsedError;
}
