import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';

import 'infrastructure/constants/environment.dart';
import 'injection.config.dart';

final getIt = GetIt.instance;

@injectableInit
void configureInjection(String environment, {String baseUrl = ''}) {
  getIt
    ..registerSingleton<String>(
      environment,
      instanceName: envInstance,
    )
    ..registerSingleton<String>(
      baseUrl,
      instanceName: baseUrlInstance,
    );
  $initGetIt(
    getIt,
    environment: environment,
  );
}

abstract class Env {
  static const development = 'development';
  static const production = 'production';
  static const staging = 'staging';
  static const all = <String>[
    development,
    production,
    staging,
  ];
}
